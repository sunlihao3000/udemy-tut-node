const express = require("express");
const bodyParser = require("body-parser");
const postRoutes = require('./routes/posts');
const mongoose = require('mongoose');
const app = express();

// connect with mongodb serve
mongoose.connect("mongodb+srv://bobby:OGgyMSelPRTNC4n1@cluster0-vzyui.mongodb.net/node-angular?retryWrites=true",{ useNewUrlParser: true })
  .then(() =>{
    console.log("Connected to database");
  })
  .catch((err ) =>{
    console.log(err);
  });
// user bodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// set header
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

app.use('/api/posts', postRoutes);


module.exports = app;
