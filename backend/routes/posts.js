const express = require("express");
const router = express.Router();
const Post = require('../models/post');

// added route /api/post
// set post request
router.post("", (req, res, next) => {
  // import the Post from mongoose model
  const post = new Post({
    title: req.body.title,
    content: req.body.content
  });
  // call save module from mongoose to save post to db
  post.save()
    .then( result =>{
      // set response message
      res.status(201).json({
        message: 'Post added successfully',
        postId: result._id
      });
    });
});
router.put("/:id",(req, res, next) => {
  const post = new Post({
    _id: req.params.id,
    title: req.body.title,
    content: req.body.content
  })
  Post.updateOne({_id: req.params.id}, post).then( result => {
    console.log(result);
    res.status(200).json({ message: "updated successful!"});
  });
});
// using find modal to find all the posts in db
router.get("", (req, res, next) => {
  Post.find()
    .then( data => {
      res.status(200).json({
        message: "Posts fetched successfully!",
        posts: data
      });
    });
});
router.get('/:id', (req,res, next) => {
  Post.findById(req.params.id).then(post => {
    if(post) {
      res.status(200).json(post);
    } else {
      res.status(404).json({message: "Post not found!"});
    }
  })
});
// using deleteOne and pass id to delete one post from db
router.delete("/:id",(req,res,next) => {
  Post.deleteOne({_id: req.params.id})
  .then(result => {
    res.status(200).json({ message: "Post delect!"});
  })
});

module.exports = router;